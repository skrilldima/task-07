package com.epam.rd.java.basic.task7.db;

import java.io.FileReader;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import com.epam.rd.java.basic.task7.db.entity.*;


public class DBManager {

	private static final String SQL_INSERT_USER = "INSERT INTO users(login) VALUES(?)";
	private static final String SQL_SELECT_ALL_USERS = "SELECT id, login FROM users order by id";
	private static final String SQL_DELETE_USERS = "DELETE FROM users WHERE id = ?";
	private static final String SQL_SELECT_USER = "SELECT id, login FROM users WHERE login = ?";
	private static final String SQL_SELECT_TEAM_BY_NAME = "SELECT id, name FROM teams WHERE name = ?";
	private static final String SQL_SELECT_ALL_TEAMS = "SELECT id, name FROM teams order by id";
	private static final String SQL_INSERT_TEAM = "INSERT INTO teams(name) VALUES(?)";
	private static final String SQL_INSERT_USERS_TEAMS = "INSERT INTO users_teams(user_id, team_id) VALUES(?,?)";
	private static final String SQL_SELECT_TEAM_FROM_USERS_TEAMS = "SELECT team_id FROM users_teams where user_id = ? order by team_id";
	private static final String SQL_DELETE_TEAM = "DELETE FROM teams WHERE name = ?";
	private static final String SQL_UPDATE_TEAM = "UPDATE teams SET name = ? WHERE id = ?";
	private static final String SQL_SELECT_TEAM_BY_ID = "SELECT id, name FROM teams where id = ?";

	private static final Properties properties = new Properties();
	private static final String DATABASE_URL;

	private static DBManager instance;

	public static synchronized DBManager getInstance() {
		if (instance == null) {
			instance = new DBManager();
		}
		return instance;
	}

	static {
		try {
			properties.load(new FileReader("app.properties"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		DATABASE_URL = properties.getProperty("connection.url");
	}

	private DBManager() {
	}

	public List<User> findAllUsers() {
		ArrayList<User> list = new ArrayList<>();
		try (Connection connection = DriverManager.getConnection(DATABASE_URL)) {
			PreparedStatement statement = connection.prepareStatement(SQL_SELECT_ALL_USERS);
			ResultSet resultSet = statement.executeQuery();
			while (resultSet.next()) {
				int id = resultSet.getInt(1);
				String login = resultSet.getString(2);
				User user = User.createUser(login);
				user.setId(id);
				list.add(user);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list;
	}

	public boolean insertUser(User user) throws DBException {
		int row = 0;
		try (Connection connection = DriverManager.getConnection(DATABASE_URL);
			 PreparedStatement statement = connection.prepareStatement(SQL_INSERT_USER, Statement.RETURN_GENERATED_KEYS)) {
			statement.setString(1, user.getLogin());
			statement.executeUpdate();
			ResultSet resultSet = statement.getGeneratedKeys();
			resultSet.next();
			user.setId(resultSet.getInt(1));
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	public boolean deleteUsers(User... users) throws DBException {
		try (Connection connection = DriverManager.getConnection(DATABASE_URL);
			 PreparedStatement statement = connection.prepareStatement(SQL_DELETE_USERS)) {
			for (int i = 0; i < users.length; i++) {
				statement.setInt(1, users[i].getId());
				statement.executeUpdate();
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	public User getUser(String login) throws DBException {
		User user = User.createUser(login);
		try (Connection connection = DriverManager.getConnection(DATABASE_URL);
			 PreparedStatement statement = connection.prepareStatement(SQL_SELECT_USER)) {
			statement.setString(1, login);
			ResultSet resultSet = statement.executeQuery();
			resultSet.next();
			user.setId(resultSet.getInt(1));
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return user;
	}

	public Team getTeam(String name) throws DBException {
		Team team = Team.createTeam(name);
		try (Connection connection = DriverManager.getConnection(DATABASE_URL);
			 PreparedStatement statement = connection.prepareStatement(SQL_SELECT_TEAM_BY_NAME)) {
			statement.setString(1, name);
			ResultSet resultSet = statement.executeQuery();
			resultSet.next();
			team.setId(resultSet.getInt(1));
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return team;
	}

	public List<Team> findAllTeams() throws DBException {
		ArrayList<Team> list = new ArrayList<>();
		try (Connection connection = DriverManager.getConnection(DATABASE_URL)) {
			PreparedStatement statement = connection.prepareStatement(SQL_SELECT_ALL_TEAMS);
			ResultSet resultSet = statement.executeQuery();
			while (resultSet.next()) {
				int id = resultSet.getInt(1);
				String name = resultSet.getString(2);
				Team team = Team.createTeam(name);
				team.setId(id);
				list.add(team);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list;
	}

	public boolean insertTeam(Team team) throws DBException {
		try (Connection connection = DriverManager.getConnection(DATABASE_URL)) {
			PreparedStatement statement = connection.prepareStatement(SQL_INSERT_TEAM, Statement.RETURN_GENERATED_KEYS);
			statement.setString(1, team.getName());
			statement.executeUpdate();
			ResultSet resultSet = statement.getGeneratedKeys();
			resultSet.next();
			team.setId(resultSet.getInt(1));
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	public boolean setTeamsForUser(User user, Team... teams) throws DBException {
		Connection connection = null;
		try {
			connection = DriverManager.getConnection(DATABASE_URL);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		try (PreparedStatement statementInsert = connection.prepareStatement(SQL_INSERT_USERS_TEAMS);) {
			connection.setAutoCommit(false);
			Savepoint save1 = connection.setSavepoint();
			try {
				for (Team team : teams) {
					statementInsert.setInt(1, user.getId());
					statementInsert.setInt(2, team.getId());
					statementInsert.executeUpdate();
				}
			} catch (SQLException e) {
				connection.rollback(save1);
				throw new DBException("DBE Exception");
			}
			connection.commit();
			return true;
		} catch (SQLException e) {
			System.out.println("GENERAL ERROR " + e.getMessage());
		} finally {
			try {
				connection.setAutoCommit(true);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return false;
	}


	public List<Team> getUserTeams(User user) throws DBException {
		List<Integer> teamIds = new ArrayList<>();
		List<Team> teams = new ArrayList<>();
		Connection connection = null;
		PreparedStatement statement = null;
		PreparedStatement statement2 = null;
		try{
			connection = DriverManager.getConnection(DATABASE_URL);
			statement = connection.prepareStatement(SQL_SELECT_TEAM_FROM_USERS_TEAMS);
			statement.setInt(1, user.getId());
			ResultSet resultSet = statement.executeQuery();
			while (resultSet.next()){
				teamIds.add(resultSet.getInt(1));
			}
			statement2 = connection.prepareStatement(SQL_SELECT_TEAM_BY_ID);
			for (Integer id:teamIds){
				statement2 = connection.prepareStatement(SQL_SELECT_TEAM_BY_ID);
				statement2.setInt(1, id);
				ResultSet resultSet2 = statement2.executeQuery();
				resultSet2.next();
				Team team = Team.createTeam(resultSet2.getString(2));
				team.setId(resultSet2.getInt(1));
				teams.add(team);
			}

		}catch (SQLException e) {
			e.printStackTrace();

		}finally {
			try {
				if (connection != null) {
					connection.close();
				}
				if (statement!=null){
					statement.close();
				}
			}catch (SQLException e){
				e.printStackTrace();
			}
		}
		return teams;

	}

	public boolean deleteTeam(Team team) throws DBException {
		try (Connection connection = DriverManager.getConnection(DATABASE_URL);
			 PreparedStatement statement = connection.prepareStatement(SQL_DELETE_TEAM)){
			statement.setString(1, team.getName());
			statement.executeUpdate();
		}catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	public boolean updateTeam(Team team) throws DBException {
		try (Connection connection = DriverManager.getConnection(DATABASE_URL);
			 PreparedStatement statement = connection.prepareStatement(SQL_UPDATE_TEAM)){
			statement.setString(1, team.getName());
			statement.setInt(2, team.getId());
			statement.executeUpdate();
		}catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

}
