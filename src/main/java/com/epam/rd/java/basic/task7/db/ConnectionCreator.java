package com.epam.rd.java.basic.task7.db;

import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class ConnectionCreator {
    private static final Properties properties = new Properties();
    private static final String DATABASE_URL;
    static {
        try {
            properties.load(new FileReader("app.properties"));
            String driverName = (String) properties.getProperty("db.driver");
            Class.forName(driverName);
        }
        catch (ClassNotFoundException | IOException e){
            e.printStackTrace();
        }
        DATABASE_URL = properties.getProperty("db.url");
    }
    private ConnectionCreator(){}
    public static Connection createConnection() throws SQLException{
        return DriverManager.getConnection(DATABASE_URL, properties);
    }
}
